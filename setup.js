/**
 * This file sets up the database for chat system
 */
const config = require('qconf')();
const debuglog = require('debug')('setup');

const rconf = config.get('RethinkdbService');
const r = require('rethinkdbdash')(rconf);

const db = 'conversation';

/**
 * list of tables to be created
 */
const tables = [
  'users',
  'consumers',
  'participants',
  'channels',
  'files',
  'messages',
];

/**
 * List of indexes on each table
 */
const indexes = {
  users: ['email', 'username'],
  consumers: ['service', 'owner', 'clientVerificationURL'],
  messages: ['channelId'],
  participants: ['channelId'],
  channels: ['title', 'createdAt', 'lastMessageAt'],
  files: [],
};


const createDb = () => r.dbCreate(db);

const createTables = () => Promise.all(tables.map(table => r.db(db).tableCreate(table)));
const createIndex = (table, index) => r.db(db).table(table).indexCreate(index);

const createIndexes = () => Promise.all(tables.map(table => indexes[table].map(index => createIndex(table, index))).reduce((acc, current) => acc.concat(current), []));
// create database
// create tables in just created database
// creates indexes on just created tables
// exit successfully if everything went ok
// log to error console in case of error
createDb()
  .then(() => createTables())
  .then(() => createIndexes())
  .then(result => debuglog('DONE! %j', result))
  .then(() => process.exit(0))
  .catch(error => debuglog('error', error));
