const EventEmitter = require('events')
const url = require('url')
const request = require('request')
const { Server } = require('ws')
const cuid = require('cuid')
const debug = require('debug')('[ChatWebSocketService]>>')

/**
 * this function takes an array of required keys and returns another function which
 * takes an object as param and return true/false if this object has all required keys
 */
const objectHasKeys = keys => qs => keys.map(key => (key in qs)).reduce((acc, current) => acc && current, true)
/**
 * Authneticate and gets user record from a remote service running on customer's host
 * @param {string} subjectVerifyUrl
 * @param {string} accessToken
 * @param {string} deviceId
 * @param {Function} callback
 */
function getUserFromAccessToken (subjectVerifyUrl, accessToken, deviceId, callback) {
  debug('verifying client  token %s on %s', accessToken, subjectVerifyUrl)
  request(`${subjectVerifyUrl}?token=${accessToken}`, (error, response, body) => {
    if (error) return callback(error)
    if (response.statusCode !== 200) {
      return callback(response.statusMessage, response, response.body)
    }
    try {
      const user = JSON.parse(body)
      Object.assign(user, { deviceId })
      return callback(null, user)
    } catch (e) {
      return callback(e)
    }
  })
}

/**
 * This function is used to authenticate a websocket client
 * websocket request should have deviceId, token, apiKey query parameters in it
 * @param {object} app
 * @return {Function} the function which is passed to ws server
 */
const authenticateClient = (app) => ({ req }, cb) => {
  const qs = url.parse(req.url, true).query

  if (!('apiKey' in qs)) {
    return cb(false, 400, 'Forbidden: apiKey missing in request')// eslint-disable-line standard/no-callback-literal
  }

  return app.getConsumerVerificationURL(qs.apiKey, (error, clientVerificationURL) => {
    if (error) return cb(false, 400, error.message) // eslint-disable-line standard/no-callback-literal
    const keys = ['deviceId', 'token', 'apiKey']
    if (objectHasKeys(keys)(qs)) {
      getUserFromAccessToken(clientVerificationURL, qs.token, qs.deviceId, (e, user) => {
        if (e) return cb(false, 401, e.message)// eslint-disable-line standard/no-callback-literal
        req.user = user
        return cb(true)// eslint-disable-line standard/no-callback-literal
      })
    } else {
      return cb(false, 400, 'Invalid connection request')// eslint-disable-line standard/no-callback-literal
    }
  })
}


function heartbeat () {
  this.isAlive = true
}

/**
 * Websocket Service is an event emitter
 * it takes app and websocket service configuration object in constructor
 * constructor calls init function to initialize websocket service
 */
class ChatWebSocketService extends EventEmitter {
  constructor (app, config) {
    super()
    this.app = app
    this.config = config
    this.init()
  }

  init () {
    const service = this
    const { config } = service
    try {
      const options = Object.assign({ port: 8000, path: '/' }, config)
      options.verifyClient = authenticateClient(this.app)

      const wss = new Server(options, () => debug('websocket server bound to port %d', options.port))
      this.wss = wss
      wss.on('connection', (ws, req) => {
        const { user } = req
        ws.isAlive = true
        service.emit('clientConnected', user, ws)
        ws.on('pong', heartbeat)
        ws.on('close', (code, message) => service.emit('clientDisconnected', user, ws, code, message))
        ws.on('message', (msg) => {
          try {
            if (process.env.NODE_ENV === 'production') {
              // parse incoming message
              const message = JSON.parse(msg)
              // identify message type and data
              const { event, data: messageData } = message
              // enhance data with correlationId, sentBy, serverRecievedAt properties
              const data = Object.assign({ correlationId: cuid() }, messageData, { sentBy: user, serverReceivedAt: Math.ceil(Date.now().valueOf() / 1000) })
              debug('emitting websocket message %j', data)
              // emit received message
              service.emit(event, data)
            } else {
              // NOTE: for cli use only, not needed in production mode
              // this block is executed in development mode
              const data = Object.assign({ correlationId: cuid() }, { sentBy: user, serverReceivedAt: Math.ceil(Date.now().valueOf() / 1000), sentAt: Math.ceil(Date.now().valueOf() / 1000) })
              const v = msg.split('|')
              const event = v[0]
              v[1].split(',').forEach((part) => {
                const [key, val] = part.split(':')
                data[key] = val
              })
              debug('emitting websocket message %j', data)
              service.emit(event, data)
            }
            // const data = { sentBy: { id: user.id, role: user.role }, deviceId: user.deviceId, serverReceivedAt: new Date(), sentAt: new Date() correlationId: cuid() }
          } catch (error) {
            // log error
            console.error(error)
          }
        })
      })

      //terminate non-responding websocket clients periodically
      setInterval(() => {
        wss.clients.forEach((ws) => {
          if (ws.isAlive === false) return ws.terminate()
          ws.isAlive = false
          ws.ping('', false, true)
        })
      }, 20000)

      process.nextTick(() => {
        this.emit('ready', wss)
      })
    } catch (error) {
      process.nextTick(() => {
        this.emit('error', error)
      })
    }
  }
}

module.exports = app => new ChatWebSocketService(app, app.config.get('ChatWebSocketService'))
