const EventEmitter = require('events')
const redis = require('redis')

class RedisService extends EventEmitter {
  constructor (redis) {
    super()
    this.redis = redis
    process.nextTick(() => {
      this.emit('ready')
    })
  }
}

module.exports = (app) => {
  const config = app.config.get('redis') || {}
  const redisClient = redis.createClient(config)
  const instance = new RedisService(redisClient)
  return instance
}
