const EventEmitter = require('events')
const debug = require('debug')('[RethinkdbService]>>')

/**
 * Rethinkdb Service is an Event emitter
 * emitting events when certain type of data
 * is added/removed
 */
class RethinkdbService extends EventEmitter {
  constructor (app, rethinkdbInstance) {
    super()
    const that = this
    rethinkdbInstance.table('participants').changes({ includeTypes: true })
      .then((stream) => {
        stream.on('data', ({ new_val, old_val, type }) => {
          switch (type) {
            case 'add':
              that.emit('channelParticipantAdded', new_val)
              break
            case 'remove':
              that.emit('channelParticipantRemoved', old_val)
              break
            default:
              debug('Unhandled type %s', type)
          }
        })
      })
      .catch(error => debug(error))
    this.rethinkdbInstance = rethinkdbInstance
    process.nextTick(() => {
      this.emit('ready')
    })
  }

  /**
   * Updates a record in participants table for last message read time in specified channel
   * @param {string} channelId
   * @param {string} userId
   * @param {string} userType
   */
  updateParticipantLastDataAccessedAtRecord (channelId, userId, userType) {
    return this.rethinkdbInstance
      .table('participants')
      .getAll(channelId, { index: 'channelId' })
      .filter({ userId, userType })
      .update({ lastDataAccessedAt: new Date() })
  }

  /**
   * Updates participants table with correlationId of last delivered message
   */
  updateLastMessageDeliveredCorrelationId ({
    channelId, correlationId: lastDeliveredCorrelationId, userId, userType
  }) {
    return this.rethinkdbInstance.table('participants').getAll(channelId, { index: 'channelId' }).filter({ userId, userType }).update({ lastDeliveredCorrelationId })
  }

  /**
   * Updates participant table record for specified input
   * with information of last read message correlation Id
   */
  updateLastMessageReadCorrelationId ({
    channelId, correlationId: lastReadCorrelationId, userId, userType
  }) {
    return this.rethinkdbInstance.table('participants').getAll(channelId, { index: 'channelId' }).filter({ userId, userType }).update({ lastReadCorrelationId })
  }

  /**
   * Stores a message in database
   * @param {object} message
   */
  async storeMessage (message) {
    const { channelId } = message
    debug('Storing message for %s: %o', channelId, message)
    try {
      await this.rethinkdbInstance.table('channels').get(channelId).update({ lastMessageAt: this.rethinkdbInstance.epochTime(message.sentAt) })
      await this.rethinkdbInstance.table('messages').insert({ ...message, sentAt: this.rethinkdbInstance.epochTime(message.sentAt), serverReceivedAt: this.rethinkdbInstance.epochTime(message.serverReceivedAt) })
      return true
    } catch (error) {
      throw error
    }
  }

  /**
   * Find list of channels of specified user
   * @param {object} user
   */
  findUserChannels (user) {
    const { id: userId, role: userType } = user
    return this.rethinkdbInstance.table('participants').filter({ userId, userType }).getField('channelId')
  }

  /**
   * Find list of participants in specified channel
   * @param {string} channelId
   */
  findChannelParticipants (channelId) {
    return this.rethinkdbInstance
      .table('participants')
      .getAll(channelId, { index: 'channelId' })
      .pluck('userId', 'userType', 'id')
  }

  /**
   * Builds rabbitmq exchange path (string) for a user
   */
  buildRabbitExchangeForUser ({ userId, userType }) {
    return `users/${userType}/${userId}`
  }

  /**
   * gets url to third party consumer service
   * where we can authenticate customers of that service
   * @param {string} apiKey
   * @param {Function} cb
   */
  getSocketClientVerificationURL (apiKey, cb) {
    this.rethinkdbInstance.table('consumers')
      .get(apiKey)
      .getField('clientVerificationURL')
      .default(null)
      .then(url => (url ? cb(null, url) : cb(new Error('No verification endpoint found for specified apiKey'))))
      .catch(error => cb(error))
  }
}

module.exports = (app) => {
  const config = app.config.get('RethinkdbService')
  const rethinkdb = require('rethinkdbdash')(config)
  const rethinkdbServiceInstance = new RethinkdbService(app, rethinkdb)
  return rethinkdbServiceInstance
}
