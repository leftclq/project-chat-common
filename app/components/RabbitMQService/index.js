const rabbitmq = require('amqplib');
const EventEmitter = require('events');
const debug = require('debug')('[RabbitMQService]>>');
/**
 * RabbitMQService is an EventEmitter
 * initialize it passing configuration object.
 * constructor calls the initializer function
 */
class RabbitMQService extends EventEmitter {
  constructor(config) {
    super();
    this.config = config;
    this.ready = false;
    // call init
    this.init();
  }


  init() {
    const {
      user,
      password,
      host,
      port,
      socketOptions,
    } = this.config;

    // connect to rabbitmq using amqp protocol
    rabbitmq.connect(`amqp://${user}:${password}@${host}:${port}`, socketOptions)
      .then(connection => connection.createConfirmChannel())
      .then((channel) => {
        this.channel = channel;
        process.nextTick(() => {
          this.ready = true;
          this.emit('ready');
        });
      }).catch((error) => {
        process.nextTick(() => {
          this.emit('error', error);
        });
      });
  }

  // publish a message to specified exchange
  publishMessage(exchange, message) {
    // publish message if queue is ready
    if (this.ready) {
      return this.channel.publish(exchange, exchange, Buffer.from(JSON.stringify(message)));
    }
    debug('not ready to publish messages yet');
    return this;
  }
  // cancels queue channel subscription for specified consumer(Tag)
  stopSendingMessagesToConsumer(consumerTag) {
    debug('cancelling subscription for tag %o', consumerTag);
    return this.channel.cancel(consumerTag);
  }

  /**
   * Prepares rabbitmq exchange/queue to receive messages
   * @param {object} user
   * @return {object} queue
   */
  async getUserMessageQueue(user) {
    try {
      // deconstruct id, role, deviceId from user object
      const { id, role, deviceId } = user;
      // build exchange string
      const exchange = `users/${role}/${id}`;
      // build queue string
      const queue = `clients/users/${role}/${id}/devices/${deviceId}`;
      // get rabbitmq channel
      const { channel } = this;
      // make sure exchange exists and is of correct type
      const ex = await channel.assertExchange(exchange, 'fanout', { durable: true });
      // make sure queue exists and is durable one
      const q = await channel.assertQueue(queue, { durable: true });
      // bind exchange to queue
      const b = await channel.bindQueue(queue, exchange, queue);
      debug('assertions: exchange %o, queue %o, binding %o DONE', ex, q, b);
      // return bound queue to caller
      return queue;
    } catch (error) {
      debug(error);
      throw error;
    }
  }

  /**
   * Sets up a connection between message queue corresponding to a user and socket resulting in forwarding of messages to socket once received from queue
   * @param {object} user
   * @param {object} socket
   * @return {Promise}
   */
  forwardMessageStream(user, socket) {
    const service = this;
    return new Promise((resolve, reject) => {
      service.getUserMessageQueue(user)
        .then(queue => service.channel.consume(queue, (msg) => {
          // convert received message from queue into string
          const message = msg.content.toString();
          // write message to socket
          socket.send(message, (error) => {
            if (error) {
              debug('Error writing to user: %s:%s socket, message was: %s', user.role, user.id, message);
              service.emit('error', error);
            } else {
              debug('Message sent to user %s:%s, socket, message was %s', user.role, user.id, message);
              service.channel.ack(msg);
            }
          });
        }))
        .then(({ consumerTag }) => resolve(consumerTag))
        .catch(error => reject(error));
    });
  }
}

module.exports = app => new RabbitMQService(app.config.get('RabbitMQService'));
