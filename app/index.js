const util = require('util')
const EventEmitter = require('events')
const rabbitMQService = require('./components/RabbitMQService')
const rethinkdbService = require('./components/RethinkdbService')
const chatWebsocketService = require('./components/ChatWebsocketService')
const redis = require('redis')

const redisClient = redis.createClient()
const debug = util.debuglog('[Conversation] >>')

/**
 * Function that creates a js object for an event with specified data
 * closes over event first
 * @param {string} event
 */
const constructEventDataMessage = event => data => ({ event, data })

//returns message type object creator function
const eventMessageDataStruct = constructEventDataMessage('message')
//returns ack type object creator function
const eventAckDataStruct = constructEventDataMessage('ack')
//returns delivered type object creator function
const eventDeliveredDataStruct = constructEventDataMessage('delivered')
//returns read type object creator function
const eventReadDataStruct = constructEventDataMessage('read')
//returns participants type object creator function
const eventParticipantsDataStruct = constructEventDataMessage('participants')
//returns newParticipant type object creator function
const eventNewParticipantDataStruct = constructEventDataMessage('newParticipant')
//returns removedParticipant type object creator function
const eventRemovedParticipantDataStruct = constructEventDataMessage('removedParticipant')

const gotError = error => debug(error)

/**
 * Main app is an event emitter
 * which accepts messages from components
 * and sends them to other components
 */

class App extends EventEmitter {
  constructor (config) {
    super()
    this.config = config
    this.rabbitConsumersRepository = new Map()
    this.webSocketConsumersRepository = new Map()
    this.on('error', gotError)
    this.on('websocketMessage', this.sendBackAcknowledgement)
    this.on('websocketMessage', this.forwardToQueue)
    this.on('websocketMessage', this.storeMessage)
    this.on('websocketMessageAck', this.ackReceivedHander)
    this.on('websocketMessageAck', this.updateLastAckToParticipantRecord)
    this.on('websocketMessageRead', this.websocketMessageReadHandler)
    this.on('websocketMessageRead', this.updateLastReadToParticipantRecord)
    this.on('websocketClientConnected', (user, socket) => {
      // when a new websocket client is connected
      // set it into repository against connected user
      this.webSocketConsumersRepository.set(user, socket)
      // connects to rabbitmq connecting component and retrieves
      // its consumer tag for later use when this particular
      // socket disconnects
      this.rabbitMQServiceInstance.forwardMessageStream(user, socket)
        .then(consumerTag => this.rabbitConsumersRepository.set(user, consumerTag))
        .catch(error => this.emit('error', error))
    })

    this.on('websocketClientConnected', (user, socket) => {
      // find user channels
      // find participants of each channel
      // sent list of participants to user in same channel
      const that = this
      // on connection initialization
      // send list of channels and channel participants
      // to connected user
      that.sendChannelsParticipants(user, socket)
        .then(() => debug('participants sent successfully for %o', user))
        .catch(error => debug('An error occured while sending data to %o: %s', user, error))
    })

    this.on('websocketClientConnected', (user) => {
      const that = this
      // hit redis cache for user's channels
      // if not available hit rethinkdb to load data
      // proceed with storing list into redis cache
      // service is 'service' whose user just connected
      // defaults to 'fixed'
      const service = 'fixed'
      const baseKey = `service:${service}:user:${user.role}:${user.id}`
      // generate search key for getting user online status
      // and subscribed channels list
      const statusKey = `${baseKey}:status`
      const channelsListKey = `${baseKey}:channels`
      debug('channelsKey', channelsListKey)

      // set current user status to online in redis cache
      redisClient.set(statusKey, 'online')
      // gets channels list from redis
      redisClient.smembers(channelsListKey, (err, reply) => {
        if (err) throw err
        debug('cache: %s ', reply)
        // hit rethinkdb if channels list is not found in redis cache
        if (!reply.length) {
          that.rethinkdbServiceInstance
            .findUserChannels(user)
            .then((channels) => {
              if (err) throw err
              // if loaded channels list is non-empty, set it into redis cache as well
              if (channels.length) {
                redisClient.sadd(channelsListKey, channels, (error, res) => {
                  if (error) throw error
                  console.info('redis cache updated', res)
                  // publish status online to channels
                  // TODO: send special 'user status' message
                })
              }
            }).catch(err => console.error(err))
        }
        return null
      })
    })
    // fires off when a websocket client gets disconnected
    this.on('websocketClientDisconnected', (user) => {
      // deduce user status key
      const service = 'fixed'
      const baseKey = `service:${service}:user:${user.role}:${user.id}`
      const statusKey = `${baseKey}:status`
      // update status to be offline
      redisClient.set(statusKey, 'offline', redis.print)
      // retrieve the just disconnected socket referenece and terminate it
      const socket = this.webSocketConsumersRepository.get(user)
      socket.terminate()
      // delete reference to just disconnected user from socket consumers repository
      this.webSocketConsumersRepository.delete(user)
      // retrieve rabbit consumer tag for just disconnected user
      const consumerTag = this.rabbitConsumersRepository.get(user)
      // stop retrieval of messages from queue
      this.rabbitMQServiceInstance.stopSendingMessagesToConsumer(consumerTag)
        // remove user from rabbitmq consumers repository
        .then(() => this.rabbitConsumersRepository.delete(user))
        .catch(error => debug(error))
    })
  }

  /**
   * App initialization procedure
   */
  init () {
    const app = this
    // connect rabbitmq service component to app component
    app.rabbitMQServiceInstance = rabbitMQService(app)
    // when rabbitmq service component sends `ready` message
    // log to console
    app.rabbitMQServiceInstance.once('ready', () => {
      debug('RabbitMQ service is ready')
      // attach event listeners
    })

    // when rabbitmq service component encounters an error
    // pass error to app
    app.rabbitMQServiceInstance.on('error', (error) => {
      app.emit('error', error)
    })

    // connect app to rethinkdbdb service component
    app.rethinkdbServiceInstance = rethinkdbService(app)

    // once rethinkdb service component is ready
    // log to console that rethinkdb service is ready
    app.rethinkdbServiceInstance.once('ready', () => {
      debug('Rethinkdb service is ready')
    })

    // when rethinkdb service encounters an error
    // pass error to app
    app.rethinkdbServiceInstance.on('error', (error) => {
      app.emit('error', error)
    })

    // when rethinkdb service emits `channelParticipantAdded` message
    // inform to each member of channel about newly added participant
    app.rethinkdbServiceInstance.on('channelParticipantAdded', (participant) => {
      debug('Added >> %o', participant)
      const { channelId } = participant
      const message = eventNewParticipantDataStruct(participant)
      app.getChannelParticipantExchanges(channelId)
        .then(exchanges => exchanges.map(exchange => app.publishMessageToExchange(exchange, message)))
        .catch(error => debug(error))
    })

    // when rethinkdb service emits `channelParticipantRemoved` message
    // inform each member of channel about just removed participant
    app.rethinkdbServiceInstance.on('channelParticipantRemoved', (participant) => {
      debug('Removed >> %o', participant)
      const { channelId } = participant
      const message = eventRemovedParticipantDataStruct(participant)
      app.getChannelParticipantExchanges(channelId)
        .then(exchanges => exchanges.map(exchange => app.publishMessageToExchange(exchange, message)))
        .catch(error => debug(error))
    })

    // connect app to websocket service component
    app.chatWebsocketServiceInstance = chatWebsocketService(app)

    // log status once websocket service component is ready
    app.chatWebsocketServiceInstance.on('ready', () => {
      debug('ChatWebsocket service is ready')
      // when websocket service component emits a `message` message
      // let app emit/distribute message to other components
      app.chatWebsocketServiceInstance.on('message', (data) => {
        app.emit('websocketMessage', data)
      })

      // when websocket service component receives a `ack`  message
      // let app emit this message so child services can act/react to it
      app.chatWebsocketServiceInstance.on('ack', (data) => {
        app.emit('websocketMessageAck', data)
      })
      // when websocket service component receives a `read`  message
      // let app emit this message so child services can act/react to it
      app.chatWebsocketServiceInstance.on('read', (data) => {
        app.emit('websocketMessageRead', data)
      })

      // when websocket service component receives a `clientConnected`  message
      // let app emit this message so child services can act/react to it
      app.chatWebsocketServiceInstance.on('clientConnected', (user, socket) => {
        app.emit('websocketClientConnected', user, socket)
      })

      // when websocket service component receives a `clientDisconnected`  message
      // let app emit this message so child services can act/react to it
      app.chatWebsocketServiceInstance.on('clientDisconnected', (user) => {
        app.emit('websocketClientDisconnected', user)
      })
    })

    // when websocket service component encounters an error
    // let app emit this error so that interested listeners can act upon it
    app.chatWebsocketServiceInstance.on('error', (error) => {
      app.emit('error', error)
    })
  }

  /**
   * Fetches list of channels of a user
   * @param {object} user
   */
  async fetchChannelsParticipants (user) {
    const app = this
    const channels = await this.rethinkdbServiceInstance.findUserChannels(user)
    const result = await Promise.all(channels.map(async (channel) => {
      const participants = await app.rethinkdbServiceInstance.findChannelParticipants(channel)
      return { channel, participants }
    }))
    return result
  }

  /**
   * Sends lists of channels participants subscribed by current user
   * @param {object} user
   * @param {object} socket
   */
  sendChannelsParticipants (user, socket) {
    return this.fetchChannelsParticipants(user)
      .then(data => new Promise((resolve, reject) => {
        const toSend = JSON.stringify(eventParticipantsDataStruct(data))
        socket.send(toSend, (error) => {
          if (error) return reject(error)
          return resolve(true)
        })
      }))
  }

  /**
   * Pushes data to rabbitmq exchange
   * @param {object} data
   */
  forwardToQueue (data) {
    const app = this
    const { channelId } = data
    const event = 'message'
    const message = { event, data }
    this.getChannelParticipantExchanges(channelId)
      .then(exchanges => exchanges.map(exchange => app.publishMessageToExchange(exchange, message)))
      .catch(error => debug(error))
  }

  /**
   * Pushes data to storage service
   * @param {object} data
   */
  storeMessage (data) {
    this.rethinkdbServiceInstance.storeMessage(data)
      .then(() => { })
      .catch(error => debug(error))
  }

  /**
   * Sends back acknowledgement to sender
   * @param {object} data
   */
  sendBackAcknowledgement (data) {
    const { sentBy } = data
    const { id: userId, role: userType } = sentBy
    const ex = this.rethinkdbServiceInstance.buildRabbitExchangeForUser({ userId, userType })
    this.publishMessageToExchange(ex, { event: 'ack', data })
  }

  /**
   * Handles a received ack message
   * basically pushes this message to all participants in channel
   * @param {object} data
   */
  ackReceivedHander (data) {
    const app = this
    const { channelId } = data
    const event = 'delivered'
    const message = { event, data }
    this.getChannelParticipantExchanges(channelId)
      .then(exchanges => exchanges.map(exchange => app.publishMessageToExchange(exchange, message)))
      .catch(error => debug(error))
  }

  /**
   * Marks  user who received the message
   * @param {object} data
   */
  updateLastAckToParticipantRecord (data) {
    const { channelId, correlationId, sentBy } = data
    const { id: userId, role: userType } = sentBy
    this.rethinkdbServiceInstance.updateLastMessageDeliveredCorrelationId({
      channelId, correlationId, userId, userType
    })
      .then(result => debug(result))
      .catch(error => debug(error))
  }

  /**
   * Marks a message as 'read'
   * @param {object} data
   */
  websocketMessageReadHandler (data) {
    const { channelId, sentBy } = data
    const { id: userId, role: userType } = sentBy
    this.rethinkdbServiceInstance.updateParticipantLastDataAccessedAtRecord(channelId, userId, userType)
      .then(result => debug(result))
      .catch(error => debug(error))

    // we also want to send this 'read' message to other channel participants

    const app = this
    const message = { event: 'read', data }
    // so we just push this read message to rabbitmq exchanges of those channel participants
    this.getChannelParticipantExchanges(channelId)
      .then(exchanges => exchanges.map(exchange => app.publishMessageToExchange(exchange, message)))
      .catch(error => debug(error))
  }

  /**
   * Updates database with information of who read a message last
   * @param {object} data
   */
  updateLastReadToParticipantRecord (data) {
    const { channelId, correlationId, sentBy } = data
    const { id: userId, role: userType } = sentBy
    this.rethinkdbServiceInstance.updateLastMessageReadCorrelationId({
      channelId, correlationId, userId, userType
    })
      .then(result => debug(result))
      .catch(error => debug(error))
  }

  /**
   * Publish a message to specified exchange
   * @param {string} exchange
   * @param {object} message
   */
  publishMessageToExchange (exchange, message) {
    this.rabbitMQServiceInstance.publishMessage(exchange, message)
  }

  /**
   * Gets list of participants in specified channel
   * @param {string} channelId
   */
  async getChannelParticipantExchanges (channelId) {
    const r = this.rethinkdbServiceInstance
    const participants = await r.findChannelParticipants(channelId)
    const exchanges = participants.map(r.buildRabbitExchangeForUser)
    return exchanges
  }

  /**
   * Returns a url to identify connecting consumer
   * apikey is used to identify the service to which key has been allocated
   * @param {string} apikey
   * @param {function} callback
   */
  getConsumerVerificationURL (apikey, callback) {
    return this.rethinkdbServiceInstance.getSocketClientVerificationURL(apikey, callback)
  }
}

module.exports = appConfig => new App(appConfig)
