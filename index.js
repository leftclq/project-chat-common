const debuglog = require('debug')('INIT')
const config = require('qconf')()
const app = require('./app')(config)

/**
 * Logs error In case an attempt is made to access an undefined setting on config object
 */
config.on('undefined', (msg, attr) => {
  debuglog('MSG: %s, Attributes: %o', msg, attr)
})

app.init()
